<?php
namespace DStaroselskiy\Wordpress\Plugins;

class PostViews
{
    public static $plugin_text_domain = 'dms_post_views';
    protected $plugin_name = 'dms_post_views';
    protected $plugin_version = '0.0.1';
    
    protected $post_views_meta_key = '_views'; // Ключ мета поля, куда будет записываться количество просмотров.
    protected $post_views_who_count = 0; // Чьи посещения считать? 0 - Всех. 1 - Только гостей. 2 - Только зарегистрированных пользователей.
    protected $post_views_exclude_bots =  1; // Исключить ботов, роботов, пауков и прочую нечесть :)? 0 - нет, пусть тоже считаются. 1 - да, исключить из подсчета.
    protected $post_views_types = array('post');
    public static $post_views_post_views = false;
    public static $post_views_cookies_name = '';
    
    const post_views_live_time = 4102444801;
    public static  function getTextDomain()
    {
        return self::$plugin_text_domain;
    }

    public function getPostViewsMetaKey()
    {
        return $this->post_views_meta_key;
    }
    protected function getPostViewsCookiesName( $post_id = '0' )
    {
        if( empty( self::$post_views_cookies_name ) )
        {
           self::$post_views_cookies_name = 'dms'.$this->getPostViewsMetaKey().( !empty( $post_id ) ? '-'.$post_id : '');
        }
        return self::$post_views_cookies_name;
    }
    protected function isPostViewsCookies( $post_id = 0 )
    {
        return isset( $_COOKIE[ $this->getPostViewsCookiesName( $post_id ) ] );
    }
    protected function setPostViewsCookies( $post_id = 0 )
    {
        \setcookie( $this->getPostViewsCookiesName( $post_id ), 'post-is-viewed', self::post_views_live_time, '/' );
    }
    public function getPostViews( $post_id = 0)
    {
       if( self::$post_views_post_views === false )
       {
          self::$post_views_post_views = (int)\get_post_meta( $post_id, $this->getPostViewsMetaKey(), true);          
       }
       return self::$post_views_post_views; 
    }
    public function setPostViews( $post_id = 0, $post_views = 0 )
    {
        if( (int)$post_id == 0 )
        {
            return false;
        }
        if( !( \update_post_meta( (int)$post_id , $this->getPostViewsMetaKey(), $post_views ) ) )
        {
            \add_post_meta( (int)$post_id , $this->getPostViewsMetaKey(), $post_views, true);
        }
        return $post_views; 
    }
    function adminInitPostViewsMetabox()
    {
	
	foreach( $this->getPostViewsType() as $post_type)
        {
            \add_meta_box('dms'.$this->getPostViewsMetaKey(), __('Views Handler'), array( &$this, 'adminShowPostViewsMetabox'), $post_type, 'side', 'high');
        }
    }
    
    public function adminShowPostViewsMetabox() 
    {
	global $post;
	$post_id = $post->ID;
	//echo '<label for="','dms'.$this->getPostViewsMetaKey(),'">',__('Количество просмотров записи'),'</label> ';
	echo '<input type="number" id= "','dms'.$this->getPostViewsMetaKey(),'" name="','dms'.$this->getPostViewsMetaKey(),'" value="'.$this->getPostViews($post_id).'" size="25" />';
	echo '<input name="save" type="submit" class="button button-primary button-large" style="margin-top: 5px;" id="publish" value="Update">';
    }

    public function adminSetPostViews($post_id) 
    {
        if ( \defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
	{	
        }
	elseif( isset( $_POST[ 'dms'.$this->getPostViewsMetaKey() ] ) )
        {
            $this->setPostViews( $post_id, (int)$_POST[ 'dms'.$this->getPostViewsMetaKey() ] );
	}
        return $post_id;
    }

    public function initPostViews()
    {
        global $user_ID, $post;
	if( \is_singular() 
                && $this->isPostViewsType( $post->post_type )
                && !$this->isPostViewsCookies( (int)$post->ID )
        ) {
            $id = (int)$post->ID;
            if( self::$post_views_post_views !== false ) return true; // чтобы 1 раз за поток
            
            $should_count = false;
            
            switch( (int)$this->post_views_who_count ) {
                case 0: $should_count = true;
                        break;
                case 1:
                        if( (int)$user_ID == 0 )
                                $should_count = true;
                        break;
                case 2:
                        if( (int)$user_ID > 0 )
                                $should_count = true;
                        break;
            }
            if( (int)$this->post_views_exclude_bots==1 && $should_count ){
                $useragent = $_SERVER['HTTP_USER_AGENT'];
                $notbot = "Mozilla|Opera"; //Chrome|Safari|Firefox|Netscape - все равны Mozilla
                $bot = "Bot/|robot|Slurp/|yahoo"; //Яндекс иногда как Mozilla представляется
                if ( !preg_match("/$notbot/i", $useragent) || preg_match("!$bot!i", $useragent) )
                        $should_count = false;
            }
            if( $should_count 
                && $this->setPostViews( $id, $this->getPostViews( $id )+1 )
            ) {
                $this->setPostViewsCookies( $id );
            }
        }
	return true;
    }
    public function setPostViewsType( $post_views_types = array( 'post' ) ) {
        $this->post_views_types = is_array( $post_views_types ) ? $post_views_types : array();
        return $this->post_views_types;
    }
    public function getPostViewsType() {
        return $this->post_views_types;
    }
    public function isPostViewsType( $post_type = '' ) {
        return in_array( $post_type, $this->post_views_types );
    }
    function initPlugin()
    {
        \add_action( 'add_meta_boxes', array( &$this, 'adminInitPostViewsMetabox') );
        \add_action( 'save_post', array( &$this, 'adminSetPostViews') );
        \add_action( 'wp_head', array( &$this, 'initPostViews') );
        \load_plugin_textdomain( self::getTextDomain(), false, __DIR__.'/languages' );    
    }
    public function __construct( $post_views_types = array( 'post' ) ) {
        $this->setPostViewsType( $post_views_types );
        \add_action('plugins_loaded', array( &$this, 'initPlugin') );
    }
    public static $pluginCore = null;
    public static function Run( $post_views_types = array( 'post' )  )
    {
        if( self::$pluginCore === null )
        {
            self::$pluginCore = new PostViews( $post_views_types );
        }
        return self::$pluginCore;
    }
    public static function getPostViewsCount( $post_id = 0  )
    {
        if( empty( $post_id ) )
        {
            global $post;
	    $post_id = (int)$post->ID;
        }
        return self::$pluginCore->getPostViews($post_id);
    }
}

?>