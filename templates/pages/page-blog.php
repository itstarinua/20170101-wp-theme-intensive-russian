<?php 
/**
 * Template Name: Display Blog
 */

use DStaroselskiy\Wordpress\Themes\ThemeController;
\load_template( ThemeController::getDir( 'archive.php' ) );
?>