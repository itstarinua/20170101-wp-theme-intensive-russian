<?php use DStaroselskiy\Wordpress\Plugins\PostViews; ?>
<?php use DStaroselskiy\Wordpress\Themes\ThemeController; ?>
<article id="blog-<?php the_ID();?>" class="blog-list blog-<?php the_ID();?>">
    <div class="article-img">
        <?php echo \get_the_post_thumbnail( null, 'blog_images', array( 'alt' => \get_the_title(), 'title' => \trim( \strip_tags( \get_the_title() ) ), ) );?>
    </div>
    <div class="article-content">
         <h4 class="article-title">
            <a href="<?php \the_permalink();?>" title="<?php \the_title();?>"><?php \the_title();?></a>
        </h4>
        <div class="cat-container">
            <?php if( class_exists('\DStaroselskiy\Wordpress\Plugins\PostViews') ): ?>
                <span class="article-views"><span class="glyphicon glyphicon-eye-open"></span><?php echo(int)PostViews::getPostViewsCount( (int)get_the_ID() ); ?></span>
            <?php endif;?>
            <?php   $cat_list = \get_the_category_list('</li><li>');
            if( empty( $cat_list ) )
            {
                $cat_list = \single_cat_title('', false);
            }
            ?><ol class="article-categories"><li><?php echo $cat_list;?></li>
        </div>
        <div class="article-excerpt">
            <?php \the_excerpt(); ?>
        </div>
        <a class="article-more-link" href="<?php \the_permalink();?>" title="<?php \the_title();?>"><span class="glyphicon glyphicon-circle-arrow-right"></span><?php _e('read more', ThemeController::getTextDomain() );?></a>
    </div>
</article>