<?php use DStaroselskiy\Wordpress\Themes\ThemeController; ?>
<article id="blog-0" class="blog-list blog-0">
    <div class="article-img">
        <?php echo \get_the_post_thumbnail( null, 'blog_images', array( 'alt' => \get_the_title(), 'title' => \trim( \strip_tags( \get_the_title() ) ), ) );?>
        <img width="504" height="301" src="<?php echo ThemeController::getUrl('images/no-content.png');?>" class="attachment-blog_images size-blog_images wp-post-image" alt="<?php _e('No contents', ThemeController::getTextDomain() );?>" title="<?php _e('No contents', ThemeController::getTextDomain() );?>">
    </div>
    <div class="article-content">
         <h4 class="article-title">
            <?php _e('Category is empty', ThemeController::getTextDomain() );?>
        </h4>
        <div class="cat-container">            
        </div>
        <div class="article-excerpt">
            <?php _e('Please, wait. Posts will be soon.', ThemeController::getTextDomain() );?>
        </div>
    </div>
</article>