/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function($){
    $(document).ready(function(){
//        new WOW().init();
        if( jQuery('body').hasClass('error404') )
        {
            h_window = jQuery(window).height();
            h_document = jQuery('html').height();
            if( h_document < h_window )
            {
                obj = jQuery('#blog-top-section');
                obj.css('padding',0);        
                padding = ( h_window - jQuery('html').height() ) / 2 ;
                obj.css('padding-top',padding+'px');        
                obj.css('padding-bottom',padding+'px');        
            }
        }
        if( typeof jQuery.prototype.selectmenu === "function" )
        {
            $select_form = jQuery( '.wpcf7-form select' );
            if( $select_form.length )
            {
                $select_form.selectmenu();
            }
        }
        
        if( typeof jQuery.prototype.datepicker === "function" )
        {
            $select_form = jQuery( '.wpcf7-form input[type="date"]' );
            if( $select_form.length )
            {
                $select_form.attr('type','input').datepicker();
            }
        }
    });
})(jQuery)

