<?php if ( \is_active_sidebar( 'sidebar-right' )  ) : ?>
    <ul class="sidebar-right-container">
        <?php global $category_widget; ?>
        <li class="cat-info widget visible-md visible-lg"><?php echo $category_widget; ?></li>
        <?php \dynamic_sidebar( 'sidebar-right' ); ?>
    </ul>
<?php endif; ?>