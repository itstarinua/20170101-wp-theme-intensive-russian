<?php
//define('DMS_THEME_DIR',__DIR__);

//define('DMS_THEME_URL',get_template_directory_uri());

error_reporting( E_ALL );
ini_set('display_errors', 1);

if( !class_exists('\DStaroselskiy\Wordpress\Themes\IntensiveRussian') ) 
{ 
    require_once 'include/ThemeCore.php'; 
    \DStaroselskiy\Wordpress\Themes\IntensiveRussian::Run();
}

//new \DStaroselskiy\Wordpress\Themes\IntensiveRussian\ThemeCore();
