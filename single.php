<?php get_header(); 
use DStaroselskiy\Wordpress\Themes\ThemeController;
use DStaroselskiy\Wordpress\Plugins\PostViews;
$current_term = get_queried_object();
$post_term_list = array();
$post_id = $have_posts = 0;
global $category_widget;
if ( \have_posts() ) {
    the_post();
    $post_id = (int)get_the_ID();
    $have_posts = true;
    $post_term_list = \wp_get_post_terms( $post_id, 'category', array("fields" => "all") );
    ob_start();
    ?>
        <h5 class="widgettitle"><?php _e('Categories ', ThemeController::getTextDomain() );?></h5>
        <?php $categorys = get_terms( 'category', array(
                'taxonomy'      => 'post',
                'orderby'       => 'name', 
                'order'         => 'ASC',
                'hide_empty'    => false, 
                'fields'        => 'all', 
                'hierarchical'  => false, 
                'get'           => 'all',
                'pad_counts'    => false, 
                'update_term_meta_cache' => true,
        ) ); ?>
        <ul class="terms-lists">
            <?php foreach ($categorys as $category) { 
                $term_select = "";
                if( count($post_term_list) > 0 ) {
                    foreach ($post_term_list as $post_term) {
                        if( $post_term->term_id == $category->term_id ) $term_select = "term-active";
                    }
                } ?>
                <li class="terms term_id-<?php echo  $category->term_id;?> term_slug-<?php echo  $category->slug;?>  <?php echo $term_select ;?>">
                    <a href="<?php echo get_term_link( $category->term_id, 'category' );?>" title="<?php echo $category->name;?>">
                        <?php echo $category->name;?>
                    </a>
                </li>
            <?php } ?>
        </ul>	
    <?php $category_widget = ob_get_clean();
}?>
<section id="blog-top-section" class="blog-top-section single-top-section ">
    <img src="<?php echo ThemeController::getUrl('images/icon_blog.jpg');?>" title="<?php ThemeController::the_header_name()?>" alt="<?php ThemeController::the_header_name()?>">
    <div class="dms-container">
        <?php ThemeController::the_header_name( '<h1 class="page-title">', '</h1>' ); ?>
        <div>
            <?php if( function_exists( 'psr_show_voting_stars' ) ) psr_show_voting_stars(); ?>
            <?php if( class_exists('DStaroselskiy\Wordpress\Plugins\PostViews') ): ?>
                <span class="article-views"><span class="glyphicon glyphicon-eye-open"></span><?php echo (int)PostViews::getPostViewsCount( (int)$post_id ); ?></span>
            <?php endif;?>
        </div>
    </div>
</section>
<!-- noindex --><ul class="categories-mobile visible-xs visible-sm">
                    <li class="cat-info widget">
                        <?php echo $category_widget; ?>
                    </li>    
<!-- /noindex --></ul>
        <main id="main" class="site-main single-section have-right-sidebar dms-container" role="main">            
            <section id="primary" class="single-container content-area">
                <?php if ( $have_posts ) : ?>
                       <?php echo \get_the_post_thumbnail( null, 'full', array( 'alt' => \get_the_title(), 'title' => \trim( \strip_tags( \get_the_title() ) ), ) );?>
                        <?php ThemeController::get_template_part( 'content',  \get_post_format() );
                        ?><div class="single-share">
                                <div class="table">
                                        <div class="tr">
                                                <div class="td"><?php _e('Понравилось? Расскажите друзьям:',ThemeController::getTextDomain());?></div>
                                                <div class="td"><iframe src="https://www.facebook.com/plugins/like.php?href=<?php echo urlencode(get_permalink());?>&width=215&layout=button_count&action=like&show_faces=true&share=true&height=46&appId" width="215" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></div>
                                        </div>
                                        <div class="tr">
                                                <div class="td"><?php _e('Оцените статью:',ThemeController::getTextDomain());?></div>
                                                <div class="td"><?php if( function_exists( 'psr_show_voting_stars' ) ) psr_show_voting_stars(); ?></div>
                                        </div>
                                </div>
                        </div> 
                            
                    <?php else :
                        ThemeController::get_template_part( 'content', 'none' );
                    endif; ?>
            </section>
            <?php get_sidebar(); ?>
        </main>
<?php get_footer(); ?>
